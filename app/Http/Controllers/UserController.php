<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;


class UserController extends Controller
{
    public function updateEmailAddress(Request $request){

        $user = User::query()->where('email',$request->new_email)->first();

        if($user){
            return response()->json(['success' => 'email address already exist']);
        }

        User::query()
            ->where('email',auth()->user()->email)
            ->update(['email' => $request->new_email]);

        return response()->json(['success' => 'email address updated successfully'],200);
    }
}
