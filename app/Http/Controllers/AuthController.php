<?php

namespace App\Http\Controllers;

use App\Mail\SendActivationMail;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Namshi\JOSE\JWT;


class AuthController extends Controller
{

    public function __construct() {
        auth()->setDefaultDriver('api');
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }

    public function sendActivationMail(Request $request){

        $toEmail = $request->email;
        $token = Str::random(60);

        $user = User::query()
            ->where('email',$request->email)
            ->first();
        if($user){

            User::query()
                ->where('email',$request->email)
                ->update([
                    'email_token' => $token,
                    'email_token_expire' => date("Y-m-d h:i:s",strtotime("+10 minutes"))
                ]);
            Mail::to($toEmail)->send(new SendActivationMail($token,$user->id));

            return response()->json(['message' => 'mail again sent successfully'],200);
        }


        $user = User::create([
            'email'    => $toEmail,
            'email_token' => $token,
            'email_token_expire' => strtotime("+10 minutes")
        ]);


        Mail::to($toEmail)->send(new SendActivationMail($token,$user->id));
        return response()->json(['message' => 'mail sent successfully'],200);
    }

    public function login(Request $request)
    {
        $token = $request->token;
        $userId = $request->userId;

        $user = User::query()
            ->where('id',$userId)
            ->first();

        if(!$user){
            return response()->json(['error' => 'user not found'],422);
        }

        $emailTokenCreateTime = strtotime($user->email_token_expire);
        $currentTime = strtotime(date('Y-m-d h:i:s'));

        if($emailTokenCreateTime - $currentTime  > 0){
            if($token == $user->email_token){

                if (!$token = auth()->login($user)) {
                    return response()->json(['error' => 'Unauthorized'], 401);
                }

                return $this->respondWithToken($token);

            }else{
                return response()->json(['error' => 'email token is invalid'],400);
            }
        }

        return response()->json(['error' => 'token time is out'],400);
    }
}
